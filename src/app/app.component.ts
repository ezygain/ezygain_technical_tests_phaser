import { Component, OnInit } from '@angular/core';
import * as Phaser from 'phaser-ce';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'EzyGain';

  game: Phaser.Game;

  logo: Phaser.Sprite;

  ngOnInit() {
    this.game = new Phaser.Game(800, 600, Phaser.AUTO, '', this);
  }

  preload() {
    console.log('preload');
    this.game.load.image('logo', 'assets/phaser.png');
  }

  create() {
    console.log('create');
    this.logo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
    this.logo.anchor.setTo(0.5, 0.5);
  }

  update() {
    console.log('update');
    this.logo.angle ++;
  }

}
