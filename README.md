# Angular Phaser

## Installation

* Install NodeJS and npm.
* Install Angular CLI : `npm install -g @angular/cli@1`. This command may need to be run as admin (using `sudo` on Linux / MacOS or administrator mode on Windows).
* In this directory, run `npm install`.

## Development

* Start the local development server using `ng serve`.
* You should now be able to access the website at `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Exercise

* You'll have to build a game using [PhaserJS](https://phaser.io/).
* You shouldn't have enough time to finish it, so the goal is to go as far as possible, and to start with the most important parts, for instance :
	* Display the background.
	* Display the cannon.
	* Rotate the cannon using the mouse's X position. The cannon should point to the left side of the sea when the mouse is at the left edge of the game, and to the right side of the sea when the mouse is at the right side of the game.
	* ...
* The game doesn't have to be exactly the same, it just has to work and to be well organized.